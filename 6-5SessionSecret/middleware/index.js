var path = require("path");
var sqlite3 = require("sqlite3");
var db = new sqlite3.Database(path.join(__dirname, "..", "cart"));

var middlewareObj = {};

//檢查登入情況
//            ++++++ 5 ++++++
middlewareObj.isLoggedIn = function(req, res, next){
    if(req.isAuthenticated())
        return next(); //進入下一個流程
    req.flash("error", "請先登入");
    res.redirect("/login");
}

module.exports = middlewareObj;