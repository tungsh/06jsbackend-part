﻿！練習題目前　重要需知！

1. 建立學號資料夾，並將所有網頁練習題組解壓縮至學號資料夾
M10423055
|-1-1ExpressServer
|-1-2Ejs
|-1-3Project
...

2. 安裝 Visual Studio Code 套件，安裝完須重新載入 Visual Studio Code 
(1) Code Runner
(2) HTML Preview
    開啟app.html檔案，快速鍵 ctrl+k h，即可看該題題目

3. 開啟整合式終端機（檢視 > 整合式終端機）

4. 在整合式終端機安裝套件至廣域，避免做下一題還需重複安裝套件
npm install body-parser -g
npm install chromedriver -g
npm install connect-flash -g
npm install ejs -g
npm install express -g
npm install express-session -g
npm install http -g
npm install method-override -g
npm install moment -g
npm install passport -g
npm install passport-local -g
npm install path -g
npm install querystring -g
npm install selenium-webdriver@3.3.0 -g
npm install sqlite3 -g
npm install bcrypt -g

5. 終端機題組目錄切換，對該題目錄 右鍵 > 在命令提示字元中開啟

6. 根據題目敘述，完成題目

7. 測試作答結果

執行 app.js 啟動 server

執行方式：
(1) Code Runner
    程式碼頁面 右鍵 Run Code
(2) 終端機
    整合式終端機 - 終端機下指令 node app.js

結束方式：
(1) Code Runner
    整合式終端機 - 輸出 右鍵 > Stop Code Run
(2) 終端機
    整合式終端機 - 終端機使用快速鍵 ctrl+c

8. 伺服器 port 相沖，終止 port 的指令
cmd 命令提示字元（右鍵 以系統管理員身分執行）
netstat -ano | findstr :yourPortNumber
taskkill /PID typeyourPIDhere /F

9. 每題作答完都執行 submit.js 提交
　提交後到 http://140.125.81.30:5000/ 看是否有提交成功

10. 課程結束後，完成所有題組並上傳至網路學園，檔名為學號
（單一檔案有限制上傳大小，要多分幾個壓縮檔上傳～）