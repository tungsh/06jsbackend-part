var express = require("express");
var router = express.Router();
var passport = require("passport");
var path = require("path");

var sqlite3 = require("sqlite3");
var db = new sqlite3.Database(path.join(__dirname, "..", "cart"));

router.get("/", function(req, res){
    res.render("landing");
});

router.get("/aboutus", function(req, res){
    res.render("aboutus");
});

router.get("/ingredient", function(req, res){
    res.render("ingredient");
});

router.get("/register", function(req, res){
   res.render("register"); 
});

router.post("/register", function(req, res){
    var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password;

    db.serialize(function() {
        db.run('INSERT INTO user VALUES (?, ?, ?)', username, email, password, 
        function(err){
            if(err){
                console.log(err);
                req.flash("error", "__________________");
                return res.redirect("register"); 
            }
            else{
                //use passport to authenticate user
                passport.authenticate("local")(req, res, function(){
                    //註冊完自動登入並導入回首頁
                    res.redirect("/"); 
                });
            }
        });
    });
});

router.get("/login", function(req, res){
    res.render("login"); 
 });

//use passport to authenticate user
router.post("/login", passport.authenticate("local", 
    {
        successRedirect: "/", //通過驗證 登入並導入回首頁
        failureRedirect: "/login", //驗證失敗 回登入畫面
        failureFlash: '__________________'
    }), function(req, res){
});

module.exports = router;