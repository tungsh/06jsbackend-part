const PORT = process.env.PORT || 3000;

var express = require("express");
var path = require("path");
var app = express();
var bodyParser = require("body-parser");

var passport = require("passport");
var LocalStrategy  = require("passport-local");
//The flash is a special area of the session used for storing messages
var flash = require("connect-flash");

var sqlite3 = require("sqlite3");
var db = new sqlite3.Database(__dirname + "/cart");

var indexRoute = require("./routes/index");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, 'views'));
app.use(express.static(__dirname + "/public"));

db.serialize(function() {
  db.run("CREATE TABLE IF NOT EXISTS user (" + 
          "username TEXT PRIMARY KEY, " + 
          "email TEXT, " +
          "password TEXT)");
});

app.use(require("express-session")({
  secret: "secret", 
  resave: false,
  saveUninitialized: false
}));

//使用flash
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(
  function(username, password, done) {
    //find user
    db.get("SELECT * FROM user WHERE username=?", username, 
    function(err, row) {
      if (!row) 
        return done(null, false); //unknown user

    //find user and match user's password
    db.get("SELECT * FROM user WHERE username=? and password=?", username, password, 
    function(err, row) {
      if (!row) 
        return done(null, false);
      return done(null, row);
    });
  });
}));

passport.serializeUser(function(user, done) {
  return done(null, user.username);
});

passport.deserializeUser(function(username, done) {
  db.get("SELECT * FROM user WHERE username=?", username,
    function(err, row) {
      if (!row)
        return done(null, false);
    return done(null, row);
  });
});

app.use(function(req, res, next){
  res.locals.currentUser = req.user;
  //flash message
  res.locals.success = req.flash('success');
  res.locals.error = req.flash('error');
  console.log(req.user);
  next();
});

app.use("/", indexRoute);

app.listen(PORT, function(){
  console.log("Starting the server port " + PORT);
})