const PORT = process.env.PORT || 3000;

var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var path = require("path");

//session passport
var passport = require("passport");
var LocalStrategy  = require("passport-local");

var sqlite3 = require("sqlite3");
var db = new sqlite3.Database(__dirname + "/cart");

var indexRoute = require("./routes/index");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, 'views'));
app.use(express.static(__dirname + "/public"));

db.serialize(function() {
  db.run("CREATE TABLE IF NOT EXISTS user (" + 
          "username TEXT PRIMARY KEY, " + 
          "email TEXT, " +
          "password TEXT)");
});

//use express-session to record user session
app.use(require("express-session")({
  secret: "secret", //session secret
  resave: false,
  saveUninitialized: false
}));

//passport初始化與使用session
app.use(passport.initialize());
app.use(passport.session());

//設定passport使用LocalStrategy
passport.use(new LocalStrategy(
  function(username, password, done) {
    //find user
    db.get("SELECT * FROM user WHERE username=?", username, 
    function(err, row) {
      if (!row) 
        return done(null, false); //unknown user

    //find user and match user's password
    db.get("SELECT * FROM user WHERE username=? and password=?", username, password, 
    function(err, row) {
      if (!row) 
        return done(null, false);
      return done(null, row);
    });
  });
}));

//序列化 存下username至user中 => req.session.passport.user
//user為session中key值
passport.serializeUser(function(user, done) {
  return done(null, user.username);
});

//反序列化 提供額外讀取user => req.user
//依據username從資料庫抓出user相關資料
passport.deserializeUser(function(username, done) {
  db.get("SELECT * FROM user WHERE username=?", username,
    function(err, row) {
      if (!row)
        return done(null, false);
    return done(null, row);
  });
});

app.use(function(req, res, next){
  //session內容
  //console.log(req.session);
  //實際存入seesion中的sessionID
  //console.log(req.sessionID);
  //將req.user值 利用res.locals 傳至ejs模板中 
  res.locals.currentUser = req.user;
  console.log(req.user);
  next();
});

app.use("/", indexRoute);

app.listen(PORT, function(){
  console.log("Starting the server port " + PORT);
})